package info.androidhive.materialtabs.activity;

import android.app.Activity;
import android.os.Bundle;
import android.text.Html;
import android.widget.TextView;

import info.androidhive.materialtabs.R;

/**
 * Created by yogeshthosare on 11/06/16.
 */

public class CookiesPolicy extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cookies_policy);
        TextView tv = (TextView) findViewById(R.id.tv1);
        tv.setText((Html.fromHtml(getString(R.string.cookies_policy))));

    }
}
