package info.androidhive.materialtabs.activity;

import android.app.Activity;
import android.os.Bundle;

import info.androidhive.materialtabs.R;

/**
 * Created by abhinav on 5/10/2016.
 */

public class AboutUs extends Activity{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.about_us);
    }
}
