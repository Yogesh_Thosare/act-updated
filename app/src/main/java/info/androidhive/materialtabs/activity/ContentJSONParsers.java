package info.androidhive.materialtabs.activity;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by abhinav on 4/23/2016.
 */
public class ContentJSONParsers{
    public static List<Content> parseFeed(String json){
        //Toast.makeText(null, json, Toast.LENGTH_SHORT).show();
        try {
            //get json object and putting it in an JSONArray
            //Content is my getter and setter.. so i will set its attributes using obj.get()
            //and put it into cont object of Content class.
            //Adding it into contentList of type List and returning it

            JSONArray ar = new JSONArray(json);
            List<Content> contentList = new ArrayList<>();
            for (int i = 0; i < ar.length(); i++){
                JSONObject obj = ar.getJSONObject(i);
                Content cont = new Content();

                cont.setType(obj.getString("type"));
                cont.setTitle(obj.getString("title"));
                cont.setDescription(obj.getString("description"));
                cont.setThumb_url(obj.getString("thumb_url"));
                cont.setRes_url(obj.getString("res_url"));
                cont.setDuration(obj.getString("duration"));
                cont.setPopularity(obj.getDouble("popularity"));
                cont.set_id(obj.getString("_id"));
                contentList.add(cont);
            }
            return contentList;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static List<Content> parseFeedOffers(String json){
        //Toast.makeText(null, json, Toast.LENGTH_SHORT).show();
        try {
            //get json object and putting it in an JSONArray
            //Content is my getter and setter.. so i will set its attributes using obj.get()
            //and put it into cont object of Content class.
            //Adding it into contentList of type List and returning it

            JSONArray ar = new JSONArray(json);
            List<Content> contentList = new ArrayList<>();
            for (int i = 0; i < ar.length(); i++){
                JSONObject obj = ar.getJSONObject(i);
                Content cont = new Content();

                cont.setDescription(obj.getString("description"));
                cont.setThumb_url(obj.getString("thumb_url"));
                cont.set_id(obj.getString("_id"));
                contentList.add(cont);
            }
            return contentList;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static List<Content> parseFeedChannel(String json){
        //Toast.makeText(null, json, Toast.LENGTH_SHORT).show();
        try {
            //get json object and putting it in an JSONArray
            //Content is my getter and setter.. so i will set its attributes using obj.get()
            //and put it into cont object of Content class.
            //Adding it into contentList of type List and returning it

            JSONArray ar = new JSONArray(json);
            List<Content> contentList = new ArrayList<>();
            for (int i = 0; i < ar.length(); i++){
                JSONObject obj = ar.getJSONObject(i);
                Content cont = new Content();

                //cont.setTitle(obj.getString("title"));
                cont.set_id(obj.getString("_id"));
                cont.setAvg_popularity(obj.getDouble("avg_popularity"));
                //cont.setDescription(obj.getString("description"));
                contentList.add(cont);
            }
            return contentList;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static List<Content> parseIdFeed(String json, Context context){

        try {
            //get json object and putting it in an JSONArray
            //Content is my getter and setter.. so i will set its attributes using obj.get()
            //and put it into cont object of Content class.
            //Adding it into contentList of type List and returning it
            JSONArray ar = new JSONArray(json);
            List<Content> contentList = new ArrayList<>();
            for (int i = 0; i < ar.length(); i++){
                JSONObject obj = ar.getJSONObject(i);
                Content cont = new Content();
                cont.setLatestId(obj.getInt("latestId"));
                cont.setNotSeen(obj.getInt("notSeen"));
               /* Toast.makeText(context, " Into parseIdFeed "+ cont.getLatestId(), Toast.LENGTH_SHORT).show();*/
                contentList.add(cont);
            }
            //Toast.makeText(context, " latest Id -  "+ cont.getLatestId(), Toast.LENGTH_SHORT).show();
            return contentList;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }
}

