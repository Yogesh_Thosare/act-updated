package info.androidhive.materialtabs.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.util.Linkify;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import cz.msebera.android.httpclient.Header;
import info.androidhive.materialtabs.R;

/**
 * Created by abhinav on 4/23/2016.
 */

public class ImageViewer extends Activity implements View.OnClickListener {
    static long vid_start;
    static boolean videoPlayPressed;
    private ImageView downloadedImg;
    private ProgressDialog simpleWaitDialog;
    //private String downloadUrl = "http://www.cleartrip.com/places/hotels/7319/731985/images/Executive_Room_2_200x200.jpg";
    private String downloadUrl = "";
    private String objId = "";
    private String videoUrl = "";
    private String t="", d="", d2="";
    private ImageView imageView;
    private ImageView ibPlayVideo;
    TextView tvtitle;
    TextView tv_desc;
    TextView link1;
    Button feedback;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.image_viewer);
        imageView = (ImageView) findViewById(R.id.imageView);
        ibPlayVideo = (ImageView) findViewById(R.id.ib_play_video);

        //feedback = (Button) findViewById(R.id.go_to_feedback);
        //feedback.setOnClickListener(this);
        imageView.setOnClickListener(this);
        ibPlayVideo.setOnClickListener(this);
        Intent intent = getIntent();
        Bundle dataBundle = intent.getExtras();
        downloadUrl = dataBundle.getString("videoThumbUrl");
        videoUrl = dataBundle.getString("videoUrl");
        //Button imageDownloaderBtn = (Button) findViewById(R.id.downloadButton);

        /**/
        downloadedImg = (ImageView) findViewById(R.id.imageView);
        tvtitle = (TextView) findViewById(R.id.tv_title1);
        tv_desc = (TextView) findViewById(R.id.tv_description);
        d=dataBundle.getString("desc");

        t=dataBundle.getString("title");

        objId = dataBundle.getString("_id");
        tvtitle.setText(t);
        /*tv_title.setBackgroundColor(Color.BLUE);*/
        try {
            d2=d.split("link1:")[0];
            tv_desc.setText(d2);
        }
        catch (Exception e){
            tv_desc.setText(d);
        }
        String l1,l2,l3;

        final TextView myClickableUrl1 = (TextView) findViewById(R.id.link1);
        try{
            l1=d.split("link1:")[1];
            l1=l1.split(":link1")[0];
            myClickableUrl1.setText(l1);
        }catch (Exception e){
            //myClickableUrl1.setText("exception1");
        }
        Linkify.addLinks(myClickableUrl1, Linkify.WEB_URLS);
        final TextView myClickableUrl2 = (TextView) findViewById(R.id.link2);
        try {
            l2 = d.split("link2:")[1];
            l2 = l2.split(":link2")[0];
            myClickableUrl2.setText(l2);
        }catch (Exception e) {
            //myClickableUrl2.setText("exception2");
        }
        Linkify.addLinks(myClickableUrl2, Linkify.WEB_URLS);
        final TextView myClickableUrl3 = (TextView) findViewById(R.id.link3);
        try {
            l3 = d.split("link3:")[1];
            l3 = l3.split(":link3")[0];
            myClickableUrl3.setText(l3);
        }catch (Exception e){
            //myClickableUrl2.setText("exception3");
        }
        Linkify.addLinks(myClickableUrl3, Linkify.WEB_URLS);


        if(dataBundle.getString("type")!=null && dataBundle.getString("type").equals("link")){
            tv_desc.setClickable(true);
            //tv_desc.setTextColor(Color.CYAN);//(Color.parseColor("#0000EE"));//(//#0000EE);
            tv_desc.setTextColor(Color.parseColor("#98dad3"));
            tv_desc.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String url = (String) tv_desc.getText();
                    //String url = (String)("https://en.wikipedia.org/wiki/Edward_Snowden");
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(url));
                    startActivity(i);
                }
            });
        }
        ImageDownloader imageDownloader = new ImageDownloader();
        imageDownloader.execute(downloadUrl);
    }

    @Override
    public void onClick(View v) {
        if (v == imageView || v == ibPlayVideo) {
            Intent intent = new Intent(android.content.Intent.ACTION_VIEW);
            Uri data = Uri.parse(videoUrl).buildUpon()
                    .appendQueryParameter("Cache-Control", "no-cache")
                    .build();
            intent.setDataAndType(data, "video/mp4");
            vid_start = System.currentTimeMillis();
            videoPlayPressed=true;
            startActivity(intent);
            /*if(System.currentTimeMillis()-vid_start>15000) {
            }*/
        }

     /*   if (v == feedback){
            Intent intent = new Intent(getApplicationContext(), Feedback.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            getApplicationContext().startActivity(intent);
        }*/
    }

    private class ImageDownloader extends AsyncTask<String, Void, Bitmap> {
        Content content;
        Bitmap bitmap = null;
        @Override
        protected Bitmap doInBackground(String... param) {
            // TODO Auto-generated method stub
            //return downloadBitmap(param[0]);
            try{
                bitmap = downloadUrl(param[0]);
            }catch(Exception e){
                Log.d("Background Task", e.toString());
            }
            return bitmap;
        }

        @Override
        protected void onPreExecute() {
            Log.i("Async-Example", "onPreExecute Called");
            simpleWaitDialog = ProgressDialog.show(ImageViewer.this,
                    "Wait", "Downloading Image");

        }

        @Override
        protected void onPostExecute(Bitmap result) {
            Log.i("Async-Example", "onPostExecute Called");
            //iv_thumbnail.setImageBitmap(cont.getBitmap());

            downloadedImg.setImageBitmap(result);
            simpleWaitDialog.dismiss();
            //finish();
        }

        private Bitmap downloadUrl(String strUrl) throws IOException {
            Bitmap bitmap=null;
            InputStream iStream = null;
            try{
                URL url = new URL(strUrl);
                /** Creating an http connection to communcate with url */
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

                /** Connecting to url */
                urlConnection.connect();

                /** Reading data from url */
                iStream = urlConnection.getInputStream();

                /** Creating a bitmap from the stream returned from the url */
                bitmap = BitmapFactory.decodeStream(iStream);

            }catch(Exception e){
                Log.d("Exception while down" , e.toString());
            }finally{
                iStream.close();
            }
            return bitmap;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        //hit count to update after 15 sec video
        //if(videoPlayPressed==true && ((System.currentTimeMillis()-vid_start)>15000)){
        if(videoPlayPressed==true && ((System.currentTimeMillis()-vid_start)>0)) {
            /*Toast.makeText(getApplicationContext(), "seen for 15 seconds",
                    Toast.LENGTH_LONG).show();*/
            /*Toast.makeText(getApplicationContext(), objId,
                    Toast.LENGTH_LONG).show();*/

            AsyncHttpClient client = new AsyncHttpClient();
            try {
                client.post("http://192.168.1.155:5000/hit_feedback?_id=" + objId + "&verify_id=lYgEwhRYl3MU75xcmdkm", new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    }
                });
            }catch (Exception e){
                //"Do nothing"
            }
            videoPlayPressed=false;
        }
    }

    @Override
    public void onBackPressed(){
        finish();
    }
}