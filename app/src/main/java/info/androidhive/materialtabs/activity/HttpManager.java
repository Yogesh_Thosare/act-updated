package info.androidhive.materialtabs.activity;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by abhinav on 4/23/2016.
 */
public class HttpManager {
    public static String getData (String uri){
        //Toast.makeText(context, "Uri -"+ uri, Toast.LENGTH_SHORT).show();
        BufferedReader reader = null;
        try {
            URL url = new URL(uri);
            URLConnection con = url.openConnection();
            StringBuilder sb = new StringBuilder();
            reader = new BufferedReader(new InputStreamReader(con.getInputStream()));

            String line;
            while((line = reader.readLine()) != null){
                sb.append(line + "\n");
            }
            return sb.toString();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }finally{
            if (reader != null){
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
    }
}
