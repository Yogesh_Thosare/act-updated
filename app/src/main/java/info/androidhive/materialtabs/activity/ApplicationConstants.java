package info.androidhive.materialtabs.activity;

import android.app.ListActivity;

/**
 * Created by abhinav on 4/23/2016.
 */

public final class ApplicationConstants extends ListActivity {
    //public static String JSON_REQUEST = "http://192.168.1.155:5000/get_list?size=70";

    //request on server
    //public static String CHANNEL_REQUEST = "http://airstacksapp.com:5000/get_channel_content?channel_name=";
    //public static String CHANNELS_LIST="http://airstacksapp.com:5000/get_channels?verify_id=lYgEwhRYl3MU75xcmdkm";
    //public static String HC_JSON_REQUEST="http://airstacksapp.com:5000/get_list?verify_id=lYgEwhRYl3MU75xcmdkm&size=60";

    //static RaPi content
    public static String CHANNEL_REQUEST = "http://airstacksapp.com:5000/get_channel_content?channel_name=";
    public static String CHANNELS_LIST="http://airstacksapp.com:5000/get_channels?verify_id=lYgEwhRYl3MU75xcmdkm&type=video";
    public static String HC_JSON_REQUEST="http://airstacksapp.com:5000/get_list?verify_id=lYgEwhRYl3MU75xcmdkm&size=60&type=video";
    public static String OFFER_JSON_REQUEST="http://airstacks.in/studio/admin/listing_offers.php";
    //public static String JSON_REQUEST = "http://airstacks.in/lat_app/menulist.json";
}