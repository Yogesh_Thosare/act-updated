package info.androidhive.materialtabs.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import info.androidhive.materialtabs.R;

/**
 * Created by abhinav on 4/23/2016.
 */

public class SplashScreen extends Activity {

    // Splash screen timer
    private static int SPLASH_TIME_OUT = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);

        new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

            @Override
            public void run() {
                //bypassing login activity
                //Intent i = new Intent(SplashScreen.this, LoginActivity.class);


                Intent i = new Intent(SplashScreen.this, IconTextTabsActivity.class);
                i.putExtra("title", "Airstacks Curated TV");
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);

                finish();
            }
        }, SPLASH_TIME_OUT);
    }
}