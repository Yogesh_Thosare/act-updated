package info.androidhive.materialtabs.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import info.androidhive.materialtabs.R;
import info.androidhive.materialtabs.fragments.MenuList;

/**
 * Created by abhinav on 4/24/2016.
 */
public class ChannelMenuList extends AppCompatActivity {
    private String jsonReq;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.channel_menu_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        Bundle dataBundle = intent.getExtras();
        jsonReq = dataBundle.getString("json_req");
        addFragment();

    }

    private void addFragment() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment fragment = new MenuList();
        Bundle bundle = new Bundle();
        bundle.putString("REQUEST",jsonReq);
        fragment.setArguments(bundle);
        fragmentManager.beginTransaction().add(R.id.container,fragment).commit();
    }
}
