package info.androidhive.materialtabs.activity;

import android.graphics.Bitmap;

/**
 * Created by abhinav on 4/23/2016.
 */
public class Content{

    private String title;
    private String description;
    private String type;
    private String res_url;
    private String thumb_url;
    private Bitmap bitmap;
    private Integer latestId;
    private Integer notSeen;
    private Bitmap image;
    private String duration;
    private String _id;

    public Double getAvg_popularity() {
        return avg_popularity;
    }

    public void setAvg_popularity(Double avg_popularity) {
        this.avg_popularity = avg_popularity;
    }

    private Double avg_popularity;



    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    private double popularity;

    public double getPopularity() {
        return popularity;
    }

    public void setPopularity(double popularity) {
        this.popularity = popularity;
    }

    public String getDuration() {
        return duration;
    }
    public void setDuration(String duration) {
        this.duration = duration;
    }
    public Bitmap getImage() {
        return image;
    }
    public void setImage(Bitmap image) {
        this.image = image;
    }
    private String mmid;
    public String getMmid() {
        return mmid;
    }
    public void setMmid(String mmid) {
        this.mmid = mmid;
    }

    public Integer getLatestId() {
        return latestId;
    }
    public void setLatestId(Integer latestId) {
        this.latestId = latestId;
    }
    public Integer getNotSeen() {
        return notSeen;
    }
    public void setNotSeen(Integer notSeen) {
        this.notSeen = notSeen;
    }
    public Bitmap getBitmap() {
        return bitmap;
    }
    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }
    public String getThumb_url() {
        return thumb_url;
    }
    public void setThumb_url(String thumb_url) {
        this.thumb_url = thumb_url;
    }
    public String getRes_url() {
        return res_url;
    }
    public void setRes_url(String res_url) {
        this.res_url = res_url;
    }
    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }




}
