package info.androidhive.materialtabs.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.content.Intent;


import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import cz.msebera.android.httpclient.Header;
import info.androidhive.materialtabs.R;

/**
 * Created by abhinav on 4/23/2016.
 */

public class Feedback extends Activity implements View.OnClickListener {

    TextView textView_name,textView_feedback;
    EditText editText_name,editText_feedback;
    Button button_submit;
    String feedback="",name="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.feedback_layout);

        textView_name=(TextView) findViewById(R.id.textView_enter_name);
        textView_feedback=(TextView) findViewById(R.id.textView_enter_feedback);

        editText_name=(EditText) findViewById(R.id.editText_enter_name);
        editText_feedback=(EditText) findViewById(R.id.editText_enter_feedback);

        button_submit=(Button) findViewById(R.id.button_submit);
        button_submit.setOnClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public void onClick(View arg0) {
        switch(arg0.getId())
        {
            case R.id.button_submit:
                //Get Input From EditText
                name=editText_name.getText().toString().trim();
                name= name.replace(' ', '+');
                feedback=editText_feedback.getText().toString().trim();
                feedback= feedback.replace('\n', ' ');
                feedback= feedback.replace("\r\n", " ");
                feedback= feedback.replace(' ', '+');
                //Validate the Input
                if(name.length()>0 && feedback.length()>0){
                    //Submit from here make post request
                    AsyncHttpClient client = new AsyncHttpClient();
                    //todo
                    client.post("http://airstacksapp.com:5000/user_feedback?name=" + name + "&comment=" + feedback + "&verify_id=lYgEwhRYl3MU75xcmdkm", new AsyncHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                            Toast.makeText(getApplicationContext(), "Thank you for your feedback!",
                                    Toast.LENGTH_LONG).show();
                        }
                        @Override
                        public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                            Toast.makeText(getApplicationContext(), "Something went wrong",
                                    Toast.LENGTH_LONG).show();
                        }
                    });
                }

                break;
        }
    }
}
