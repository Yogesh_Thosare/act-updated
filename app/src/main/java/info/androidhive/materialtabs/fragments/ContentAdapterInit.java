package info.androidhive.materialtabs.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import info.androidhive.materialtabs.R;
import info.androidhive.materialtabs.activity.ApplicationConstants;
import info.androidhive.materialtabs.activity.ChannelMenuList;
import info.androidhive.materialtabs.activity.Content;

/**
 * Created by abhinav on 4/24/2016.
 */

public class ContentAdapterInit extends ArrayAdapter<Content> {
    private Context context;
    private List<Content> ChannelList;
    public ContentAdapterInit (Context context, int resource, List<Content> objects){
        super(context, resource, objects);
        this.context = context;
        this.ChannelList = objects;
    }

    @Override
    public View getView (final int position, View convertView, ViewGroup parent){
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.item_content_init, parent, false);
        //Typeface tfr = Typeface.createFromAsset(context.getAssets(), "Roboto-Regular.ttf");
        //Typeface tft = Typeface.createFromAsset(context.getAssets(), "Roboto-Thin.ttf");
        final TextView tv_title = (TextView) view.findViewById(R.id.textView1);
        TextView tv_des = (TextView) view.findViewById(R.id.textView2);
        //tv_des.setTypeface(tft);
        //tv_title.setTypeface(tfr);
        final Content cont = ChannelList.get(position);


        //tv_title.setText(cont.getTitle());
        tv_title.setText(cont.get_id());



        tv_des.setText(cont.getDescription());

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //String rJSON = JSON_REQUEST + cont.getRes_url();
                Intent intent = new Intent(context, ChannelMenuList.class);
//              intent.putExtra("json_req", "http://airstacks.in/lat_app/menulist.json");
//                intent.putExtra("json_req", ApplicationConstants.CHANNELS_LIST);
                intent.putExtra("json_req", ApplicationConstants.CHANNEL_REQUEST + cont.get_id() + "&verify_id=lYgEwhRYl3MU75xcmdkm&size=60&type=video");
                //intent.putExtra("title", cont.getTitle());
                context.startActivity(intent);
            }
        });
        return view;
    }
}
