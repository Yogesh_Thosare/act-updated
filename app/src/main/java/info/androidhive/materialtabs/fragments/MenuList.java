package info.androidhive.materialtabs.fragments;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import info.androidhive.materialtabs.R;
import info.androidhive.materialtabs.activity.ApplicationConstants;
import info.androidhive.materialtabs.activity.Content;
import info.androidhive.materialtabs.activity.ContentJSONParsers;
import info.androidhive.materialtabs.activity.HttpManager;

public class MenuList extends Fragment {

    public MenuList() {
        // Required empty public constructor
    }
    String JSON;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null) {
            JSON = bundle.getString("REQUEST");
        } else {
            JSON = ApplicationConstants.HC_JSON_REQUEST;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //return inflater.inflate(R.layout.fragment_one, container, false);
        return inflater.inflate(R.layout.activity_main, container, false);
    }
    ImageView imgview;
    ListView list;
    TextView output;
    ProgressBar pb;
    List<MyTask> tasks;
    List<Content> contentList;

    //List<Content> contentListTot=new ArrayList<Content>();
    RelativeLayout layout,layout2;



    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        layout = (RelativeLayout)view.findViewById(R.id.contListLayout);
        pb = (ProgressBar)view.findViewById(R.id.progressBar1);
        output = (TextView)view.findViewById(R.id.unreachable);
        imgview = (ImageView) view.findViewById(R.id.img1);
        imgview.setVisibility(View.INVISIBLE);
        pb.setVisibility(View.INVISIBLE);
        list = (ListView)view.findViewById(R.id.list);


        /*setContentView(R.layout.activity_main);
        Intent intent = getIntent();
        Bundle dataBundle = intent.getExtras();*/
        //String title = dataBundle.getString("title");
        //Typeface tfr = Typeface.createFromAsset(getAssets(), "Roboto-Regular.ttf");



        tasks = new ArrayList<>();
        if (isOnline()) {
            //commented for temperory testing
            /*String JSON = dataBundle.getString("json_req");
            requestData(JSON);*/
            requestData(JSON);
            //requestData("http://192.168.2.10/myjson/demom.json");
            //requestData("http://192.168.0.102/myjson/demom.json");
            //requestData("http://192.168.0.110:8080/channel/content");
        } else {
            Toast.makeText(getActivity(), "Network is not available", Toast.LENGTH_LONG)
                    .show();
        }
    }

    private void requestData(String uri) {
        MyTask task = new MyTask();
        //Toast.makeText(getApplicationContext(), "going in do-in-bg URI " + uri,Toast.LENGTH_LONG);
        task.execute(uri);

    }

    protected void updateDisplay() {
        ContentAdapter adapter = new ContentAdapter(getActivity(), R.layout.item_content, contentList);
        list.setAdapter(adapter);
    }

    protected boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        } else {
            return false;
        }
    }
    public class MyTask extends AsyncTask<String, String, List<Content>> {
        @Override
        protected void onPreExecute() {
//			updateDisplay("Starting task");
            /*Toast.makeText(getApplicationContext(), "going in do-in-bg",
                    Toast.LENGTH_LONG).show();*/
            if (tasks.size() == 0) {
                pb.setVisibility(View.VISIBLE);
            }
            tasks.add(this);

        }
        @Override
        protected List<Content> doInBackground(String... params) {
            String json = HttpManager.getData(params[0]);
            try {
                contentList = ContentJSONParsers.parseFeed(json);
            }catch (Exception e){
                return null;
            }

            /*for (Content content : contentList){
                try{

                    //String thumbnail_url = THUMBNAIL_URL + content.getThumb_url();
                    //Log.d("Tag", thumbnail_url);
                    String thumbnail_url = content.getThumb_url();
                    InputStream in =(InputStream) new URL(thumbnail_url).getContent();
                    Bitmap bitmap = BitmapFactory.decodeStream(in);
                    content.setBitmap(bitmap);
                    in.close();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }*/
            return contentList;
            //contentListTot.addAll(contentList);
            //return contentListTot;
        }
        @Override
        protected void onPostExecute(List<Content> result) {
            //contentList = ContentJSONParsers.parseFeed(result);
            if(result==null){
                pb.setVisibility(View.INVISIBLE);
                output.setVisibility(View.VISIBLE);
                imgview.setVisibility(View.VISIBLE);
            }else {
                output.setVisibility(View.INVISIBLE);
                imgview.setVisibility(View.INVISIBLE);
                updateDisplay();
                // updateDisplay(result);
                tasks.remove(this);
                if (tasks.size() == 0) {
                    pb.setVisibility(View.INVISIBLE);
                }
            }
        }
        @Override
        protected void onProgressUpdate(String... values) {
//			updateDisplay(values[0]);
        }

    }
}
