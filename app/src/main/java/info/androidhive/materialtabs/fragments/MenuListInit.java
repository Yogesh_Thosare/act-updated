package info.androidhive.materialtabs.fragments;

/**
 * Created by abhinav on 4/24/2016.
 */

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import info.androidhive.materialtabs.R;
import info.androidhive.materialtabs.activity.ApplicationConstants;
import info.androidhive.materialtabs.activity.Content;
import info.androidhive.materialtabs.activity.ContentJSONParsers;
import info.androidhive.materialtabs.activity.HttpManager;

public class MenuListInit extends Fragment {


    public MenuListInit() {
        // Required empty public constructor
    }
    //private static final String THUMBNAIL_URL = "http://services.hanselandpetal.com/photos/";
    //private static final String THUMBNAIL_URL = "http://192.168.0.102/myjson/";
    //private static final String THUMBNAIL_URL = "http://192.168.0.110:1028/get/";
    ListView list;
    TextView output;
    ProgressBar pb;
    List<MyTask> tasks;
    ImageView imgview;
    List<Content> contentList;

    //List<Content> contentListTot=new ArrayList<Content>();
    RelativeLayout layout,layout2;
    //MyTask task = new MyTask();
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //return inflater.inflate(R.layout.fragment_one, container, false);
        return inflater.inflate(R.layout.activity_main, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        layout = (RelativeLayout)view.findViewById(R.id.contListLayout);
        output = (TextView)view.findViewById(R.id.unreachable);
        imgview = (ImageView) view.findViewById(R.id.img1);
        pb = (ProgressBar)view.findViewById(R.id.progressBar1);
        imgview.setVisibility(View.INVISIBLE);
        pb.setVisibility(View.INVISIBLE);
        list = (ListView)view.findViewById(R.id.list);

        tasks = new ArrayList<>();
        if (isOnline() == true) {
            ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(getContext().CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            if(netInfo != null && netInfo.isConnectedOrConnecting()){
                //requestData("http://airstacks.in/videos/init.json");
                requestData(ApplicationConstants.CHANNELS_LIST);
            }else{
                Toast.makeText(getActivity(), "Network is not available", Toast.LENGTH_LONG)
                        .show();
            }
            //String JSON = dataBundle.getString("json_req");
            //requestData(JSON);
            //requestData("http://192.168.1.179/myjson/demom_init.json");
            //requestData("http://192.168.0.102/myjson/demom.json");
            //requestData("http://192.168.0.110:8080/channel/content");
        } else {
            Toast.makeText(getActivity(), "Network is not available", Toast.LENGTH_LONG)
                    .show();
        }
    }

    private void requestData(String uri) {
        MyTask task = new MyTask();
        task.execute(uri);
    }

    protected void updateDisplay() {
        ContentAdapterInit adapter = new ContentAdapterInit(getActivity(), R.layout.item_content, contentList);
        list.setAdapter(adapter);
    }

    protected boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        } else {
            return false;
        }
    }
    public class MyTask extends AsyncTask<String, String, List<Content>> {
        @Override
        protected void onPreExecute() {
//			updateDisplay("Starting task");
            /*Toast.makeText(getApplicationContext(), "going in do-in-bg",
                    Toast.LENGTH_LONG).show();*/
            if (tasks.size() == 0) {
                pb.setVisibility(View.VISIBLE);
            }
            tasks.add(this);
        }
        @Override
        protected List<Content> doInBackground(String... params) {
            //change made on 12-08-2015
            //String json = HttpManager.getData(params[0]);
            if (isOnline()) {
                String json = HttpManager.getData(params[0]);
                try {
                    contentList = ContentJSONParsers.parseFeedChannel(json);
                }catch (Exception e){
                    return null;
                }
                //return contentList;
            } else {
                Toast.makeText(getActivity(), "Network is not available", Toast.LENGTH_LONG)
                        .show();
            }
            //

            //contentList = ContentJSONParsers.parseFeed(json);
            return contentList;
        }
        @Override
        protected void onPostExecute(List<Content> result) {
            //contentList = ContentJSONParsers.parseFeed(result);
            if(result==null){
                pb.setVisibility(View.INVISIBLE);
                output.setVisibility(View.VISIBLE);
                imgview.setVisibility(View.VISIBLE);
            }else {
                output.setVisibility(View.INVISIBLE);
                imgview.setVisibility(View.INVISIBLE);
                updateDisplay();
                // updateDisplay(result);
                tasks.remove(this);
                if (tasks.size() == 0) {
                    pb.setVisibility(View.INVISIBLE);
                }
            }
        }
        @Override
        protected void onProgressUpdate(String... values) {
//			updateDisplay(values[0]);
        }

    }
}