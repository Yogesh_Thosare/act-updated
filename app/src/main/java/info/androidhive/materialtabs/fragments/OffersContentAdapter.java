package info.androidhive.materialtabs.fragments;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import java.io.InputStream;
import java.net.URL;
import java.util.List;

import info.androidhive.materialtabs.R;
import info.androidhive.materialtabs.activity.Content;

/**
 * Created by abhinav on 4/23/2016.
 */
/*interface Fragment{
    public void onCreate(Bundle savedInstanceState);
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState);
}*/

public class OffersContentAdapter extends ArrayAdapter<Content> {

    private Context context;
    private List<Content> videoList;

    public OffersContentAdapter (Context context, int resource, List<Content> objects){
        super(context, resource, objects);
        this.context = context;
        this.videoList = objects;
    }

    /*public ContentAdapter() {
        // Required empty public constructor
    }*/

    @Override
    public View getView (final int position, View convertView, ViewGroup parent){
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        ViewGroup ctnr = null;
        View view = inflater.inflate(R.layout.offer_item_content, ctnr, false);
        TextView tv_title = (TextView) view.findViewById(R.id.textView1);
        TextView tv_duration = (TextView) view.findViewById(R.id.textDuration);
        TextView tv_des = (TextView) view.findViewById(R.id.textView2);
        RatingBar ratingbar = (RatingBar) view.findViewById(R.id.MyRating);

        final Content cont = videoList.get(position);
        //tv_title.setText(cont.getTitle());
        tv_title.setText(cont.getDescription());
        //tv_duration.setText(cont.getDuration());
        //ratingbar.setRating((float) cont.getPopularity());
            /*ratingbar.setRating(3.5f);*/
        if(cont.getBitmap()!=null) {
            ImageView iv_thumbnail = (ImageView) view.findViewById(R.id.imageView1);
            iv_thumbnail.setImageBitmap(cont.getBitmap());
        }else {
            ThumbnailLazyLoader container = new ThumbnailLazyLoader();
            container.content = cont;
            container.view = view;

            ImageLoader loader = new ImageLoader();
            loader.execute(container);
        }

/*        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(context, "Opening - " + cont.getRes_url(), Toast.LENGTH_LONG).show();
                if(cont.getType().equals("image")) {
                    Intent intent = new Intent(context, ImageViewer.class);
                    intent.putExtra("mediaURL_tag1", cont.getRes_url());
                    intent.putExtra("title",cont.getTitle());
                    intent.putExtra("desc",cont.getDescription());
                    context.startActivity(intent);
                }
                if(cont.getType().equals("audio")||cont.getType().equals("video")|| true){
                    //trying native app
                    Intent intent = new Intent(context, ImageViewer.class);
                    intent.putExtra("videoThumbUrl", cont.getThumb_url());
                    intent.putExtra("videoUrl", cont.getRes_url());
                    intent.putExtra("title",cont.getTitle());
                    intent.putExtra("desc",cont.getDescription());
                    intent.putExtra("_id",cont.get_id());
                    context.startActivity(intent);
                }
                if(cont.getType().equals("link")){
                    Intent intent = new Intent(context, ImageViewer.class);
                    intent.putExtra("mediaURL_tag1", cont.getThumb_url());
                    intent.putExtra("title",cont.getTitle());
                    intent.putExtra("type","link");
                    intent.putExtra("desc",cont.getRes_url());
                    context.startActivity(intent);
                }
            }
        });*/

        return view;
    }
    class ThumbnailLazyLoader{
        public Content content;//setter and getter is in content
        public View view;
        public Bitmap bitmap;
    }
    private class ImageLoader extends AsyncTask<ThumbnailLazyLoader, Void, ThumbnailLazyLoader> {

        @Override
        protected ThumbnailLazyLoader doInBackground(ThumbnailLazyLoader... params) {
            ThumbnailLazyLoader container = params[0];
            Content content = container.content;
            try{
                String thumbnail_url = content.getThumb_url();
                InputStream in =(InputStream) new URL(thumbnail_url).getContent();
                Bitmap bitmap = BitmapFactory.decodeStream(in);
                content.setBitmap(bitmap);
                in.close();
                container.bitmap = bitmap;
                return container;
            }catch (Exception e){
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(ThumbnailLazyLoader result) {
            if(result!=null) {
                ImageView iv_thumbnail = (ImageView) result.view.findViewById(R.id.imageView1);
                //iv_thumbnail.setColorFilter(Color.argb(150, 155, 155, 155),   Mode.SRC_ATOP);
                iv_thumbnail.setColorFilter(Color.argb(10, 105, 105, 105));

                iv_thumbnail.setImageBitmap(result.bitmap);
                result.content.setBitmap(result.bitmap);
            }
        }
    }
}