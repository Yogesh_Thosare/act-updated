// Generated code from Butter Knife. Do not modify!
package info.androidhive.materialtabs.activity;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.Injector;

public class LoginActivity$$ViewInjector<T extends info.androidhive.materialtabs.activity.LoginActivity> implements Injector<T> {
  @Override public void inject(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131493011, "field '_emailText'");
    target._emailText = finder.castView(view, 2131493011, "field '_emailText'");
    view = finder.findRequiredView(source, 2131493013, "field '_passwordText'");
    target._passwordText = finder.castView(view, 2131493013, "field '_passwordText'");
    view = finder.findRequiredView(source, 2131493014, "field '_loginButton'");
    target._loginButton = finder.castView(view, 2131493014, "field '_loginButton'");
    view = finder.findRequiredView(source, 2131493015, "field '_signupLink'");
    target._signupLink = finder.castView(view, 2131493015, "field '_signupLink'");
    view = finder.findRequiredView(source, 2131493012, "field '_mobileNum'");
    target._mobileNum = finder.castView(view, 2131493012, "field '_mobileNum'");
  }

  @Override public void reset(T target) {
    target._emailText = null;
    target._passwordText = null;
    target._loginButton = null;
    target._signupLink = null;
    target._mobileNum = null;
  }
}
